package ltd.vblago.taskbp1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.widget.TextView;

import java.util.Locale;

import ltd.vblago.taskbp1.model.User;

public class ResultActivity extends AppCompatActivity {

    TextView nameView;
    TextView ageView;
    TextView daysView;
    TextView secondsView;
    TextView zodiacView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result);

        nameView = findViewById(R.id.name);
        ageView = findViewById(R.id.age);
        daysView = findViewById(R.id.days);
        secondsView = findViewById(R.id.seconds);
        zodiacView = findViewById(R.id.zodiac);

        Intent intent = getIntent();

        User user = (User) intent.getExtras().getSerializable("user");

        nameView.setText(String.format("%s, статистика по вашему дню рождения %s :",
                user.name, DateUtils.formatDateTime(this,
                user.birthday.getTimeInMillis(),
                DateUtils.FORMAT_NUMERIC_DATE)));
        ageView.setText(String.format(Locale.US,"Ваш возраст: %d", user.getAge()));
        daysView.setText(String.format(Locale.US,"Количество прожитых дней: %d", user.getDays()));
        secondsView.setText(String.format(Locale.US,"Количество прожитых секунд: %d", user.getSeconds()));
        zodiacView.setText(String.format("Ваш зодиак: %s", user.getZodiacSign()));
    }
}
