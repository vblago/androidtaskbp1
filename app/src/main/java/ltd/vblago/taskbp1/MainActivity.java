package ltd.vblago.taskbp1;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;
import java.util.GregorianCalendar;

import ltd.vblago.taskbp1.model.User;

public class MainActivity extends AppCompatActivity {

    EditText nameEdit;
    TextView birthdayView;
    Button statusBtn;
    Calendar birthday;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nameEdit = findViewById(R.id.name_edit);
        birthdayView = findViewById(R.id.birthday_view);
        statusBtn = findViewById(R.id.status_btn);

        birthday = Calendar.getInstance();

        birthdayView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDate(view);
            }
        });

        statusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToResultActivity();
            }
        });
    }

    public void goToResultActivity(){
        Intent intent = new Intent(this, ResultActivity.class);
        User user = new User(nameEdit.getText().toString(), (GregorianCalendar) birthday);
        intent.putExtra("user", user);
        startActivity(intent);
    }

    public void setDate(View v) {
        new DatePickerDialog(MainActivity.this, datePickerDialogListener,
                birthday.get(Calendar.YEAR),
                birthday.get(Calendar.MONTH),
                birthday.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    private void setInitialDateTime() {
        birthdayView.setText(DateUtils.formatDateTime(this,
                birthday.getTimeInMillis(),
                DateUtils.FORMAT_NUMERIC_DATE));
    }

    DatePickerDialog.OnDateSetListener datePickerDialogListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            birthday.set(Calendar.YEAR, year);
            birthday.set(Calendar.MONTH, monthOfYear);
            birthday.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setInitialDateTime();
        }
    };
}
