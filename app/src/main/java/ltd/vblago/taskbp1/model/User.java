package ltd.vblago.taskbp1.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class User implements Serializable {
    public String name;
    public GregorianCalendar birthday;

    public User(String name, GregorianCalendar birthday) {
        this.name = name;
        this.birthday = birthday;
    }

    public int getAge() {
        GregorianCalendar checkDay = new GregorianCalendar();
        int years = checkDay.get(GregorianCalendar.YEAR) - birthday.get(GregorianCalendar.YEAR);
        // корректируем, если текущий месяц в дате проверки меньше месяца даты рождения
        int checkMonth = checkDay.get(GregorianCalendar.MONTH);
        int birthMonth = birthday.get(GregorianCalendar.MONTH);
        if (checkMonth < birthMonth) {
            years--;
        } else if (checkMonth == birthMonth
                && checkDay.get(GregorianCalendar.DAY_OF_MONTH) < birthday.get(GregorianCalendar.DAY_OF_MONTH)) {
            // отдельный случай - в случае равенства месяцев,
            // но меньшего дня месяца в дате проверки - корректируем
            years--;
        }
        return years;
    }

    public int getDays() {
        GregorianCalendar now = new GregorianCalendar();
        long millis = now.getTimeInMillis() - birthday.getTimeInMillis();
        return (int) (millis / (24 * 60 * 60 * 1000));
    }

    public int getSeconds() {
        GregorianCalendar now = new GregorianCalendar();
        long millis = now.getTimeInMillis() - birthday.getTimeInMillis();
        return (int) (millis / 1000);
    }

    public String getZodiacSign() {
        String zodiac = "";
        double date = birthday.get(Calendar.MONTH) + 1 + (((double) birthday.get(Calendar.DAY_OF_MONTH) + 1) / 100);
        if (1.21 <= date && date <= 02.19) {
            zodiac = "Водолей";
        } else if (2.20 <= date && date <= 3.20) {
            zodiac = "Рыбы";
        } else if (3.21 <= date && date <= 4.20) {
            zodiac = "Овен";
        } else if (4.21 <= date && date <= 5.21) {
            zodiac = "Телец";
        } else if (5.22 <= date && date <= 6.21) {
            zodiac = "Близнецы";
        } else if (6.22 <= date && date <= 7.23) {
            zodiac = "Рак";
        } else if (7.24 <= date && date <= 8.23) {
            zodiac = "Лев";
        } else if (8.24 <= date && date <= 9.23) {
            zodiac = "Дева";
        } else if (9.24 <= date && date <= 10.23) {
            zodiac = "Весы";
        } else if (10.24 <= date && date <= 11.22) {
            zodiac = "Скорпион";
        } else if (11.23 <= date && date <= 12.21) {
            zodiac = "Стрелец";
        } else if (12.22 >= date || date <= 01.20) {
            zodiac = "Козерог";
        }

        return zodiac;
    }
}
